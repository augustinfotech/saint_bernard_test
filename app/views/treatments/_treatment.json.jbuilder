json.extract! treatment, :id, :patient_id, :description, :necessity, :created_at, :updated_at
json.url treatment_url(treatment, format: :json)
