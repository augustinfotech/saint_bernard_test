class PatientTreatment < ApplicationRecord
  belongs_to :patient, optional: true
  belongs_to :treatment, optional: true
end
